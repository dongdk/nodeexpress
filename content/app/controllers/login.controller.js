const LoginService = require('../services/login.services')    

// Create and Save a new Account
exports.create = (req, res) => {
    LoginService.create(req, res);  
  };

// Retrieve all Account from the database.
exports.findAll = (req, res) => {
    LoginService.findAll(req, res);  
  };

// Find a single Account with an id
exports.findOne = (req, res) => {
    LoginService.findOne(req, res);  
  };

// Update a Account by the id in the request
exports.update = (req, res) => {
  LoginService.update(req, res);  
  };

// Delete a Account with the specified id in the request
exports.delete = (req, res) => {
    LoginService.delete(req, res);
  };

// Delete all Accounts from the database.
exports.deleteAll = (req, res) => {
    LoginService.deleteAll(req, res);
  };

// Find all published Accounts
exports.findAllPublished = (req, res) => {
    LoginService.findAllPublished(req, res);
  };