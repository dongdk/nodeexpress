module.exports = (sequelize, Sequelize) => {
    const Account = sequelize.define("account", {
      title: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.STRING
      },
      published: {  
        type: Sequelize.BOOLEAN
      }
    });
  
    return Account;
  };