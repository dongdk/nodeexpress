module.exports = app => {
    const Accounts = require("../controllers/login.controller.js");
  
    var router = require("express").Router();
  
    // Create a new Accounts
    router.post("/", Accounts.create);
  
    // Retrieve all Account
    router.get("/", Accounts.findAll);
  
    // Retrieve all published Accounts
    router.get("/published", Accounts.findAllPublished);
  
    // Retrieve a single Account with id
    router.get("/:id", Accounts.findOne);
  
    // Update a Account with id
    router.put("/:id", Accounts.update);
  
    // Delete a Account with id
    router.delete("/:id", Accounts.delete);
  
    // Delete all Accounts
    router.delete("/", Accounts.deleteAll);
  
    app.use('/api/accounts', router);
  };